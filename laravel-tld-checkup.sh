#!/bin/bash
#
# Usage: laravel-tld-checkup.sh [-h]
# This script can be used to check /var/www/laravel directory and creat the needed entry in hosts file and nginx config.
#
#	-h, --help	Display this help-message and exit

# help message
function usage ()
{
cat << EOF
Usage: $0 [-h]
This script can be used to check /var/www/laravel directory and creat the needed entry in hosts file and nginx config.

	-h, --help	Display this help message and exit
EOF
}


if (( $# == 1 )) || (( $# == 0 )); then
	[[ ! -z $1 ]] || [[ $1 == "-h" ]] || [[ $1 == "--help" ]] && usage && exit 0  # check for -h flag
    for entry in $(ls /var/www/laravel)
        do
            isInFile=$(cat /etc/hosts | grep -c ${entry}.laravel)
            if [ $isInFile -eq 0 ]; then
                echo 127.0.0.1 ${entry}.laravel >> /etc/hosts
                # add nginx config to site-avaliable and site-enabled
                cat > /etc/nginx/sites-available/${entry}.laravel<< EOF
server {
    listen 80;
    server_name ${entry}.laravel;
    root /var/www/laravel/${entry}/public;

    add_header X-Frame-Options "SAMEORIGIN";
    add_header X-XSS-Protection "1; mode=block";
    add_header X-Content-Type-Options "nosniff";

    index index.html index.htm index.php;

    charset utf-8;

    location / {
        try_files \$uri \$uri/ /index.php?\$query_string;
    }

    location = /favicon.ico { access_log off; log_not_found off; }
    location = /robots.txt  { access_log off; log_not_found off; }

    error_page 404 /index.php;

    location ~ \.php$ {
        fastcgi_pass unix:/var/run/php/php8.0-fpm.sock;
        fastcgi_index index.php;
        fastcgi_param SCRIPT_FILENAME \$realpath_root\$fastcgi_script_name;
        include fastcgi_params;
    }

    location ~ /\.(?!well-known).* {
        deny all;
    }
}
EOF
                
                ln -s /etc/nginx/sites-available/${entry}.laravel /etc/nginx/sites-enabled/
                service nginx reload
            fi
        done
elif (( $# > 1 ));then
	usage
	exit 11
fi

